﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Envirobots_Environment : MonoBehaviour {

    [SerializeField] public EnvironmentSettings settings;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init(EnvironmentSettings s)
    {
        settings = s;
        if (s.bgImage == null)
            GameManager.instance.ui.bgImage.color = s.bgColor;
        else
        {
            GameManager.instance.ui.bgImage.color = new Color(1f, 1f, 1f, 1f);
            GameManager.instance.ui.bgImage.sprite = s.bgImage;
        }
    }
}
