﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

class CreateEnvironmentSettings
{
    public class MakeScriptableObject
    {
        [MenuItem("Assets/Create/Environment Setting")]
        public static void CreateMyAsset()
        {
            EnvironmentSettings asset = ScriptableObject.CreateInstance<EnvironmentSettings>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/EnvironmentSettings/EnvironmentSetting.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }
    }
}
#endif