﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Environment Setting", menuName = "Environment Settings", order = 1)]
public class EnvironmentSettings : ScriptableObject {

    /// <summary>
    /// Maps the name of assets to a float value for an environment
    /// </summary>
    public List<EnvironmentKVP> assetValueMap;
    /// <summary>
    /// bgColor for debugging. Replace with bg sprite later
    /// </summary>
    public Color bgColor;
    /// <summary>
    /// theBGImage
    /// </summary>
    public Sprite bgImage;


}
[System.Serializable]
public class EnvironmentKVP
{
    public string assetName { get { return asset.name; } }
    public ModuleType assetType { get { return asset.GetComponent<Module>().type; } }
    public GameObject asset;
    [Range(0f,1f)]
    public float assetValue;

    public EnvironmentKVP (GameObject o, float v)
    {
        asset = o;
        assetValue = v;
    }

}
