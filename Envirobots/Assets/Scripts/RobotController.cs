﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

public class RobotController : MonoBehaviour {

    public const int numberOfParts = 5;

    public static RobotController instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<RobotController>();
            return _instance;
        }
    }

    static RobotController _instance;
    
    GameObject[] _robotParts;
    public GameObject GetRobotPart (char c)
    {
        return instance._robotParts[(int)c - startChar];
    }
    public List<GameObject> GetRobotParts ()
    {
        List<GameObject> output = new List<GameObject>();

        for (int i = 0; i < _robotParts.Length; i++)
        {
            output.Add(_robotParts[i]);
        }

        return output;
    }
    const int startChar = 67;
    private void Awake()
    {
        
        _instance = this;
        partmap = new Dictionary<char, string>();
        reversepartmap = new Dictionary<string, char>();
        equippedpartmap_transforms = new Dictionary<ModuleType, Transform>
        {
            { ModuleType.Chasse, null },
            { ModuleType.Energy, null },
            { ModuleType.Vision, null },
            { ModuleType.Utility, null },
            { ModuleType.Movement, null }
        };
        Module[] robo_cache= Resources.LoadAll<Module>("RobotParts");
        _robotParts = new GameObject[robo_cache.Length];
        for (int i = 0; i < robo_cache.Length; i++)
        {
            _robotParts[i] = robo_cache[i].gameObject;
        }
        equippedpartmap_chars = new Dictionary<ModuleType, char>
        {
            { ModuleType.Chasse, '_' },
            { ModuleType.Energy, '_' },
            { ModuleType.Vision, '_' },
            { ModuleType.Utility, '_' },
            { ModuleType.Movement, '_' }
        };

        for (int i = startChar; i < startChar + _robotParts.Length; i++)
        {
            partmap.Add((char)i, _robotParts[i - startChar].name);
            reversepartmap.Add(_robotParts[i - startChar].name, (char)i);
        }        
    }

    public Transform energyTransform;
    public Transform visionTransform;
    public Transform utilityTransform;
    public Transform movementTransform;
    public Transform chasseTransform;

    [SerializeField] GameObject MainBodyObject;

    public static Dictionary<char, string> partmap;
    public static Dictionary<string, char> reversepartmap;
    public static Dictionary<ModuleType, Transform> equippedpartmap_transforms;

    internal static void SetEquippedPart(ModuleType type, string name)
    {
        equippedpartmap_chars[type] = reversepartmap[name];

        Debug.Log("Setting " + type.ToString() + " to " + reversepartmap[name]);
    }

    public static Dictionary<ModuleType, char> equippedpartmap_chars;


    /// <summary>
    /// Builds the first robot from equippedpartmap_chars
    /// </summary>
    public void BuildFirstRobot ()
    {
        MainBodyObject.SetActive(true);

        BuildRobot(new Dictionary<ModuleType, char>(equippedpartmap_chars));
    }

    public char[] GetEquippedParts()
    {
        char[] output = new char[5];

        for (int i = 0; i < RobotController.numberOfParts; i++)
        {
            ModuleType type = ModuleType.Chasse;
            switch (i)
            {
                case 0:                    
                    type = ModuleType.Chasse;
                    break;
                case 1:
                    type = ModuleType.Vision;
                    break;
                case 2:
                    type = ModuleType.Energy;
                    break;
                case 3:
                    type = ModuleType.Movement;
                    break;
                case 4:
                    type = ModuleType.Utility;
                    break;
            }
            output[i] = equippedpartmap_chars[type];
        }

            return output;
    }

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(new Vector3(0, 10f * Time.deltaTime, 0));
	}
    
    public void SetPart(ModuleType moduleType, char partID)
    {
        Transform trns = GetPivot(moduleType);

        GameObject toSet = GetRobotPart(partID);
        GameObject instantiated = Instantiate(toSet);
        instantiated.transform.position = new Vector3(0, 0, 0);
        instantiated.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        instantiated.transform.SetParent(trns, false);

        

        equippedpartmap_chars[moduleType] = partID;
        equippedpartmap_transforms[moduleType] = instantiated.transform;
    } 


   
    void BuildRobot(Dictionary<ModuleType, string> buildlist)
    {
        foreach (ModuleType t in buildlist.Keys)
        {
            GameObject toSet = GetRobotPart(partmap.Keys.First(v => partmap[v] == buildlist[t]));
            
            Transform trns = GetPivot(t);
            if (trns == null)
                throw new System.Exception("you had too many part names in the list you passed to the BuildRobot function");


            GameObject instantiated = Instantiate(toSet);
            instantiated.transform.position = new Vector3(0, 0, 0);
            instantiated.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            instantiated.transform.SetParent(trns, false);

            
            equippedpartmap_chars[t] = reversepartmap[buildlist[t]];
            equippedpartmap_transforms[t] = instantiated.transform;
        }
        
    }

    internal void RecycleRobot()
    {
        List<ModuleType> eqmp = equippedpartmap_chars.Keys.ToList();
        for (int i = 0; i < eqmp.Count; i++)
        {
            if (equippedpartmap_transforms[eqmp[i]] == null)
                continue;
            try
            {
                GameObject toDestroy = equippedpartmap_transforms[eqmp[i]].gameObject;
                Destroy(toDestroy);
                equippedpartmap_transforms[eqmp[i]] = null;
                equippedpartmap_chars[eqmp[i]] = '_';
            }
            catch (UnityException e)
            {
                Debug.Log(e);
            }
        }

       
    }

    /// <summary>
    /// first loads energy, then vision, then utility, then movement left, then movement right
    /// </summary>
    /// <param name="prefabNames"></param>
    public void BuildRobot(Dictionary<ModuleType, char> buildlist)
    {
        foreach (ModuleType t in buildlist.Keys)
        {
            if (buildlist[t] == '_') continue;
            GameObject toSet = GetRobotPart(buildlist[t]);            

            Transform trns = GetPivot(t);
            if (trns == null)
                throw new System.Exception("you had too many part names in the list you passed to the BuildRobot function");


            GameObject instantiated = Instantiate(toSet);
            instantiated.transform.position = new Vector3(0, 0, 0);
            instantiated.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            instantiated.transform.SetParent(trns, false);



            equippedpartmap_transforms[t] = instantiated.transform;
            equippedpartmap_chars[t] = buildlist[t];
        }
    }

    private Transform GetPivot(ModuleType t)
    {
        Transform trns = null;
        switch (t)
        {
            case ModuleType.Energy:
                trns = energyTransform;
                break;
            case ModuleType.Vision:
                trns = visionTransform;
                break;
            case ModuleType.Utility:
                trns = utilityTransform;
                break;
            case ModuleType.Movement:
                trns = movementTransform;
                break;
            case ModuleType.Chasse:
                trns = chasseTransform;
                break;
            default:
                trns = null;
                break;
        }
        return trns;
    }

    /// <summary>
    /// Converts a char list to a list of strings by the partmap dictionary
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    public List<string> GetListFromEncodedChars(List<char> keys)
    {
        List<string> output = new List<string>();

        
            foreach (char c in keys)
            {
                output.Add(partmap[c]);
            }


        return output;
    }

    public float AssertFit (EnvironmentSettings e)
    {
        float output = 0f;


        foreach (Transform t in equippedpartmap_transforms.Values)
        {
            if (t == null) continue;
            foreach (EnvironmentKVP kvp in e.assetValueMap)
            {
                //pretty hacky currently assuming the name of the instantiated object is always at beginning of string.
                
                if (t.name.StartsWith(kvp.assetName))
                {
                    output += kvp.assetValue;
                    
                }
            }
        }
        output /= (float)(numberOfParts - 3);
        return output;
    }
    
    /*
     * Es wechseln sich immer mindestens n Teile.
     * am besten angepasste Teile werden mit hoher Wahrscheinlichkeit bevorzugt.
     * 
     * */

    
 
}
