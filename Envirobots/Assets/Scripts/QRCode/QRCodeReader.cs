﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using ZXing;
using ZXing.QrCode;

public class QRCodeReader : MonoBehaviour {

    private WebCamTexture camTexture;
    private Rect screenRect;

    private Texture2D myQR;
    private string lastScanText = "";

    void Start()
    {
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = 256;//Screen.height;
        camTexture.requestedWidth = 256;//Screen.width;
        myQR = generateQR("test");
        if (camTexture != null)
        {
            camTexture.Play();
        }

        //InvokeRepeating("DecodeQR", 2.0f, 0.3f);
    }

    void OnGUI()
    {
        // drawing the camera on screen
        GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
        // do the reading — you might want to attempt to read less often than you draw on the screen for performance sake
        try
        {
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            //Debug.Log(camTexture.width);
            var result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
            if (result != null)
            {
                Debug.Log("DECODED TEXT FROM QR: " +result.Text);
                lastScanText = result.Text;
            }
            barcodeReader = null;
        }
        catch (Exception ex) {
            Debug.LogWarning(ex.Message);
        }
        

        if (GUI.Button(new Rect(0, 0, 256, 256), myQR )) { }
        if (GUI.Button(new Rect(0, 0, Screen.width, Screen.height/12), lastScanText)) { }
    }

    private void Update()
    {
        
    }

    public Texture2D generateQR(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    private void DecodeQR()
    {

    }
}
