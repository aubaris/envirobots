﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ZXing;
using ZXing.QrCode;

public class QRDataManager : MonoBehaviour {

    public static event Action<QRDataManager> QRDataManagerAwoken;
    static event Action<string> QRCodeScanned;
    List<Action<string>> actionsToExecute;
    Coroutine the_routine;
    WebCamTexture camTexture;
    string fetchedQRResult;
    bool abortScan;
    public void Awake()
    {
        actionsToExecute = new List<Action<string>>();
        QRCodeScanned += QRDataManager_QRCodeScanned;
        if (QRDataManagerAwoken != null)
            QRDataManagerAwoken(this);

        abortScan = false;
    }
    private void Start()
    {

    }
    private void OnDestroy()
    {
        QRCodeScanned -= QRDataManager_QRCodeScanned;
    }

    /// <summary>
    /// Fetch a QR code by scanning with the camera and execute the action passed to this function.
    /// </summary>
    /// <param name="resultAction">the action to execute once scanning is finished</param>
    public void FetchQRCode (Action<string> resultAction)
    {
        QRCodeScanned += resultAction;
        actionsToExecute.Add(resultAction);
        abortScan = false;
        if (the_routine != null)
            StopCoroutine(the_routine);

        the_routine = StartCoroutine(ScanQR());
    }

    public Texture2D GetQRCodeFromString (string s)
    {
        return generateQR(s);
    }

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    Texture2D generateQR(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    internal void AbortScan()
    {
        StopCoroutine(the_routine);
        the_routine = null;
    }

    private void QRDataManager_QRCodeScanned(string obj)
    {
        foreach (Action<string> a in actionsToExecute)
        {
            QRCodeScanned -= a;
        }

        actionsToExecute.Clear();
    }

    IEnumerator ScanQR ()
    {
        Result result = null;
        
        while (result == null && !abortScan)
        {
            IBarcodeReader barcodeReader = new BarcodeReader();

            try
            {
                // decode the current frame
                //Debug.Log(camTexture.width);
                result = barcodeReader.Decode(GameManager.i.ui.camTexture.GetPixels32(), GameManager.i.ui.camTexture.width, GameManager.i.ui.camTexture.height);

            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex.Message);

            }
            barcodeReader = null;

            yield return null;
        }
        Debug.Log(result.Text);
        abortScan = false;
        if (QRCodeScanned != null)
            QRCodeScanned(result.Text);

        the_routine = null;
    }
}
