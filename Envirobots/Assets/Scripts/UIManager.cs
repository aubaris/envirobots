﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class UIManager : MonoBehaviour {

    public static event Action<UIManager> UIManagerAwoken;

    public event Action ScanningStarted;
    public event Action ScanningWindowHidden;
    public event Action QRshown;
    public event Action QRScanAborted;

    public event Action ConfigMovementButtonClicked;
    public event Action ConfigEnergyButtonClicked;
    public event Action ConfigVisionButtonClicked;
    public event Action ConfigUtilityButtonClicked;

    public event Action LeftChangeButtonClicked;
    public event Action RightChangeButtonClicked;

    public event Action PartConfirmButtonClicked;

    public event Action MainMenuOpened;

    public RawImage camImage;
    public RawImage generatedQRImage;
    public GameObject qrPanel;

    public GameObject selectPanel;

    public Image bgImage;
    public WebCamTexture camTexture;


    [Header("Menus")]
    public GameObject panelCustomize;
    public GameObject panelMain;
    public GameObject panelQRShow;
    public GameObject panelQRScan;

    private void Awake()
    {
        camTexture = new WebCamTexture(256, 256);
        if (UIManagerAwoken != null)
            UIManagerAwoken(this);
    }
    // Use this for initialization
    void Start () {
        
        StartCoroutine(tryConnectToCam());
        
    }


    IEnumerator tryConnectToCam()
    {
        yield return null;
        camImage.texture = camTexture;
        camImage.material.mainTexture = camTexture;
        yield return null;
        qrPanel.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update () {
	}

    public void ShowCam()
    {
        qrPanel.gameObject.SetActive(true);
        camTexture.Play();
    }
    public void HideCam()
    {
        qrPanel.gameObject.SetActive(false);
        GameManager.instance.qrmanager.AbortScan();
        camTexture.Stop();
    }
    public void SetBGImage (Sprite s)
    {
        bgImage.sprite = s;
    }

#region MENU
    public void OpenMenuCustomize()
    {
        panelCustomize.SetActive(true);
        selectPanel.SetActive(false);
        panelMain.SetActive(false);
        panelQRScan.SetActive(false);
        panelQRShow.SetActive(false);
    }

    public void OpenMenuMain()
    {
        //if (!GameManager.i.constructionComplete)
        //    return;

        if (MainMenuOpened != null)
            MainMenuOpened();

        panelCustomize.SetActive(false);
        selectPanel.SetActive(false);
        panelMain.SetActive(true);
        panelQRScan.SetActive(false);
        panelQRShow.SetActive(false);

        
    }

    public void OpenMenuQRScan()
    {
        panelCustomize.SetActive(false);
        selectPanel.SetActive(false);
        panelMain.SetActive(false);
        panelQRScan.SetActive(true);
        panelQRShow.SetActive(false);
        if (ScanningStarted != null)
            ScanningStarted();
    }

    public void CloseMenuQRScan()
    {
        panelQRScan.SetActive(false);
        OpenMenuMain();
    }

    public void CloseMenuQRShow ()
    {
        panelQRShow.SetActive(false);
        OpenMenuMain();
    }

    public void AbortQRScan()
    {
        if (QRScanAborted != null)
            QRScanAborted();

        CloseMenuQRScan();
    }

    public void OpenMenuQRShow()
    {
        selectPanel.SetActive(false);

        panelCustomize.SetActive(false);
        panelMain.SetActive(false);
        panelQRScan.SetActive(false);
        panelQRShow.SetActive(true);
        if (QRshown != null)
            QRshown();
    }

    public void SetQRCode (Texture2D qrTexture)
    {
        generatedQRImage.texture = qrTexture;
    }

    public void ClickEnergyConfigButton ()
    {
        ShowSelectPanel();
        if (ConfigEnergyButtonClicked != null)
            ConfigEnergyButtonClicked();
    }

    public void ClickMovementConfigButton ()
    {
        ShowSelectPanel();
        if (ConfigMovementButtonClicked != null)
            ConfigMovementButtonClicked();
    }

    public void ClickVisionConfigButton ()
    {
        ShowSelectPanel();
        if (ConfigVisionButtonClicked != null)
            ConfigVisionButtonClicked();
    }

    public void ClickUtilityConfigButton ()
    {
        ShowSelectPanel();
        if (ConfigUtilityButtonClicked != null)
            ConfigUtilityButtonClicked();
    }

    public void ClickLeftChangeButton ()
    {
        if (LeftChangeButtonClicked != null)
            LeftChangeButtonClicked();
    }
    public void ClickRightChangeButton ()
    {
        if (RightChangeButtonClicked != null)
            RightChangeButtonClicked();
    }

    public void ClickPartConfirmButton ()
    {
        if (PartConfirmButtonClicked != null)
            PartConfirmButtonClicked();
    }
    public void ShowSelectPanel ()
    {
        selectPanel.SetActive(true);
        panelCustomize.SetActive(false);
        panelMain.SetActive(false);
        panelQRScan.SetActive(false);
        panelQRShow.SetActive(false);

    }
    #endregion
}
