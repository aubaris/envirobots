﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager i { get { return instance; } }
    public static GameManager instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();

            return _instance;
        }
    }
    static GameManager _instance;

    public QRDataManager qrmanager;
    public UIManager ui;

    Envirobots_Environment environment;
    Transform partSelectPlateTransform;

    List<GameObject> currentSelectableModules;
    GameObject currentLoadedModule;
    int currentSelectedIndex;

    bool _gameRunning;
    bool _robotLoaded;
    const float waitInterval = 1.0f;

    public bool constructionComplete
    {
        get
        {
            return false;
        }
    }

    // Percentage bar for environment success
    public Image envPercentageImage;

    private void Awake()
    {
        currentSelectedIndex = 0;
        _robotLoaded = false;
        partSelectPlateTransform = GameObject.Find("PartSelectPlate").transform;
        if (_instance == null)
        {
            Init();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    void Init()
    {
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
        UIManager.UIManagerAwoken += UIManager_UIManagerAwoken;
        NetworkMaster.ConnectionComplete += NetworkMaster_ConnectionComplete;
        qrmanager = this.gameObject.AddComponent<QRDataManager>();
        NetworkMaster.RecievedTransmission += NetworkMaster_RecievedTransmission;
        currentSelectableModules = new List<GameObject>();
    }

    private void NetworkMaster_ConnectionComplete()
    {
        StartGame();
    }

    void SubscribeUIEvents()
    {
        ui.ScanningStarted += Ui_ScanningStarted;
        ui.QRScanAborted += Ui_QRScanAborted;
        ui.QRshown += Ui_QRshown;

        ui.ConfigEnergyButtonClicked += Ui_ConfigEnergyButtonClicked;
        ui.ConfigMovementButtonClicked += Ui_ConfigMovementButtonClicked;
        ui.ConfigUtilityButtonClicked += Ui_ConfigUtilityButtonClicked;
        ui.ConfigVisionButtonClicked += Ui_ConfigVisionButtonClicked;

        ui.LeftChangeButtonClicked += Ui_LeftChangeButtonClicked;
        ui.RightChangeButtonClicked += Ui_RightChangeButtonClicked;

        ui.PartConfirmButtonClicked += Ui_PartConfirmButtonClicked;

        ui.MainMenuOpened += Ui_MainMenuOpened;
    }

    private void Ui_MainMenuOpened()
    {
        if  (!_robotLoaded)
        {
            RobotController.instance.BuildFirstRobot();
            NetworkMaster.CreateRoom();
            _robotLoaded = true;
        }
    }

    private void Ui_PartConfirmButtonClicked()
    {
        Module t = currentLoadedModule.GetComponent<Module>();

        RobotController.SetEquippedPart(t.type, t.name);

        Destroy(currentLoadedModule);
        currentSelectedIndex = 0;
        currentSelectableModules.Clear();
        ui.OpenMenuCustomize();
    }

    private void Ui_RightChangeButtonClicked()
    {
        currentSelectedIndex++;
        if (currentSelectedIndex > currentSelectableModules.Count - 1)
            currentSelectedIndex = 0;

        LoadSelectedModule();
    }

    private void Ui_LeftChangeButtonClicked()
    {
        currentSelectedIndex--;
        if (currentSelectedIndex < 0)
            currentSelectedIndex = currentSelectableModules.Count - 1;

        LoadSelectedModule();

    }

    void LoadSelectedModule ()
    {
        Destroy(currentLoadedModule);
        float scalingFactor = 1.5f;
        currentLoadedModule = Instantiate(currentSelectableModules[currentSelectedIndex], partSelectPlateTransform);
        Bounds currentLoadedModuleBounds = GetChildRendererBounds(currentLoadedModule);
        currentLoadedModule.transform.localScale = new Vector3(scalingFactor / (currentLoadedModuleBounds.extents.x), scalingFactor / (currentLoadedModuleBounds.extents.x), scalingFactor / (currentLoadedModuleBounds.extents.x));
        currentLoadedModuleBounds = GetChildRendererBounds(currentLoadedModule);
        Debug.Log(currentLoadedModuleBounds.extents);
        Debug.Log(currentLoadedModuleBounds.center);
        currentLoadedModule.transform.position = new Vector3(0,-currentLoadedModuleBounds.center.y,  Math.Abs (currentLoadedModuleBounds.center.z) );
        currentLoadedModule.transform.rotation = Quaternion.Euler(10, 180, 0);
        currentLoadedModule.name = currentSelectableModules[currentSelectedIndex].name;
    }

    Bounds GetChildRendererBounds(GameObject go)
    {
        Renderer[] renderers = go.GetComponentsInChildren<Renderer>();

        if (renderers.Length > 0)
        {
            Bounds bounds = renderers[0].bounds;
            for (int i = 1, ni = renderers.Length; i < ni; i++)
            {
                bounds.Encapsulate(renderers[i].bounds);
            }
            return bounds;
        }
        else
        {
            Renderer goRenderer = go.GetComponent<Renderer>();
            if (goRenderer == null)
            {
                return new Bounds();
            }
            else
            {
                return goRenderer.bounds;
            }
            
        }
    }

    void LoadConfigModels (ModuleType t)
    {
        currentSelectableModules.Clear();
        currentSelectedIndex = 0;
        foreach (GameObject g in RobotController.instance.GetRobotParts())
        {
            if (g.GetComponent<Module>().type == t)
                currentSelectableModules.Add(g);
        }

        LoadSelectedModule();
    }

    private void Ui_ConfigVisionButtonClicked()
    {
        LoadConfigModels(ModuleType.Vision);
    }

    private void Ui_ConfigUtilityButtonClicked()
    {
        LoadConfigModels(ModuleType.Utility);
    }

    private void Ui_ConfigMovementButtonClicked()
    {
        LoadConfigModels(ModuleType.Movement);
    }

    private void Ui_ConfigEnergyButtonClicked()
    {
        LoadConfigModels(ModuleType.Energy);
    }

    private void Ui_QRshown()
    {
        ui.SetQRCode(qrmanager.GetQRCodeFromString(NetworkMaster.UID.ToString()));
    }

    private void Ui_QRScanAborted()
    {
        qrmanager.AbortScan();
    }

    private void Ui_ScanningStarted()
    {
        ui.ShowCam();
        qrmanager.FetchQRCode(UpdateBot);
    }

    private void UpdateBot(string obj)
    {
        ui.CloseMenuQRScan();

        NetworkMaster.InitiateTransaction(obj);
        CreateChildRobot(NetworkMaster.GetRobostringByHash(obj));


    }

    private void NetworkMaster_RecievedTransmission(string otherString, ExitGames.Client.Photon.Hashtable roomdata)
    {
        NetworkDebugging.Log("Recieved message: " + otherString);

        CreateChildRobot(otherString);

        ui.CloseMenuQRShow();
        NetworkMaster.UpdateRobostring(new string(RobotController.equippedpartmap_chars.Values.ToArray()));
    }

    private void Start()
    {
    }

    /// <summary>
    /// Starts a new game. Requires atleast one EnvironmentSettings in Assets/Resources/EnvironmentSettings
    /// </summary>
    public void StartGame()
    {
        environment = gameObject.AddComponent<Envirobots_Environment>();

        EnvironmentSettings[] settings = Resources.LoadAll<EnvironmentSettings>("EnvironmentSettings");

        if (settings.Length <= 0)
        {
            Debug.LogError("No Environment Settings in Resources/EnvironmentSettings aborting game start");
            return;
        }

        int rndIndex = UnityEngine.Random.Range(0, settings.Length);

        environment.Init(settings[rndIndex]);

        NetworkMaster.UpdateRobostring(new string(RobotController.equippedpartmap_chars.Values.ToArray()));

        //insert check if server or client!
        StartCoroutine(GameUpdate());
    }

    public void TestCamActivation ()
    {
        ui.ShowCam();
    }
    public void TestCamDeactivation ()
    {
        ui.HideCam();
    }

    private void UIManager_UIManagerAwoken(UIManager obj)
    {
        ui = obj;
        SubscribeUIEvents();

    }
    public void TestQRScan ()
    {
        qrmanager.FetchQRCode(qrFetched);
    }
    /// <summary>
    /// This function must recieve a char array with the following order:
    /// 
    /// [0] = Chasse,
    /// [1] = Vision,
    /// [2] = Energy,
    /// [3] = Movement,
    /// [4] = Utility
    /// </summary>
    void qrFetched (string s)
    {
        Debug.Log(s);
        CreateChildRobot(NetworkMaster.GetRobostringByHash(s));
        NetworkMaster.InitiateTransaction(s);

        ui.HideCam();
        NetworkMaster.UpdateRobostring(new string(RobotController.equippedpartmap_chars.Values.ToArray()));
    }

    /// <summary>
    /// Start this to enable ticks.
    /// </summary>
    /// <returns></returns>
    IEnumerator GameUpdate ()
    {
        yield return new WaitForSeconds(2f);
        _gameRunning = true;
        while (_gameRunning)
        {
            envPercentageImage.fillAmount = RobotController.instance.AssertFit(environment.settings);
            Debug.Log("Currently adapted " + RobotController.instance.AssertFit(environment.settings) * 100 + "%");
            yield return new WaitForSeconds(waitInterval);
        }
    }

    /// <summary>
    /// This function must recieve a char array with the following order:
    /// 
    /// [0] = Chasse,
    /// [1] = Vision,
    /// [2] = Energy,
    /// [3] = Movement,
    /// [4] = Utility
    /// </summary>
    /// <param name="otherParentData"></param>
    public void CreateChildRobot(string otherParentData)
    {
        char[] myparts = RobotController.instance.GetEquippedParts();
        Dictionary<ModuleType, char> newRobotData = new Dictionary<ModuleType, char>();

        for (int i = 0; i < RobotController.numberOfParts; i++)
        {

            ModuleType type = ModuleType.Chasse;
            switch (i)
            {
                case 0:
                    type = ModuleType.Chasse;
                    break;
                case 1:
                    type = ModuleType.Vision;
                    break;
                case 2:
                    type = ModuleType.Energy;
                    break;
                case 3:
                    type = ModuleType.Movement;
                    break;
                case 4:
                    type = ModuleType.Utility;
                    break;
            }

            List<EnvironmentKVP> ekvp = new List<EnvironmentKVP>(environment.settings.assetValueMap);
            EnvironmentKVP myKvp = null;
            EnvironmentKVP otherKvp = null;

            if (myparts.ElementAt(i) != '_')
            {
                foreach (EnvironmentKVP kvp in ekvp)
                {

                    if (RobotController.partmap[myparts.ElementAt(i)] == kvp.assetName)
                    {
                        myKvp = kvp;
                    }
                }
            }
            if (otherParentData[i] != '_')
            {
                foreach (EnvironmentKVP kvp in ekvp)
                {
                    if (RobotController.partmap[otherParentData[i]] == kvp.assetName)
                    {
                        otherKvp = kvp;
                    }
                }

            }

            if (myKvp == null || otherKvp == null)
            {
                newRobotData.Add(type, myparts[i]);
                continue;
            }
            if (myKvp.assetValue > otherKvp.assetValue)
            {
                if (UnityEngine.Random.Range(0f, 1f) < 0.99f)
                {
                    if (myparts[i] != '_')
                        newRobotData.Add(type, myparts[i]);
                }
                else
                {
                    if (otherParentData[i] != '_')
                        newRobotData.Add(type, otherParentData[i]);
                }
            }
            else
            {
                if (UnityEngine.Random.Range(0f, 1f) < 0.99f)
                {
                    if (otherParentData[i] != '_')
                        newRobotData.Add(type, otherParentData[i]);
                }
                else
                {
                    if (myparts[i] != '_')
                        newRobotData.Add(type, myparts[i]);
                }
            }
        }
        
        RobotController.instance.RecycleRobot();
        if (newRobotData.Count > 0)
            RobotController.instance.BuildRobot(newRobotData);
    }

    public void TestRobotOptimisation()
    {
        char[] rndchars = new char[RobotController.numberOfParts];
        List<char> possibleChars = RobotController.partmap.Keys.ToList();

        for (int i = 0; i < RobotController.numberOfParts; i++)
        {
            ModuleType type = ModuleType.Chasse;
            switch (i)
            {
                case 0:
                    rndchars[i] = '_';
                    continue;
                    type = ModuleType.Chasse;
                    break;
                case 1:
                    type = ModuleType.Vision;
                    break;
                case 2:
                    type = ModuleType.Energy;
                    break;
                case 3:
                    type = ModuleType.Movement;
                    break;
                case 4:
                    type = ModuleType.Utility;
                    break;
            }

            Module m = null;
            while (m == null || m.type != type)
            {
                rndchars[i] = possibleChars[UnityEngine.Random.Range(0, possibleChars.Count())];
                m = RobotController.instance.GetRobotPart(rndchars[i]).GetComponent<Module>();
            }
        }

        
        CreateChildRobot(new string(rndchars));
    }
}
