﻿#define NETWORK_DEBUGGING_ENABLED

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkDebugging : MonoBehaviour {

    static NetworkDebugging i;

    public GameObject msgPrefab;
    public Transform msgParent;

    private void Awake()
    {
        i = this;
    }

    public static void Log(string msg)
    {
#if NETWORK_DEBUGGING_ENABLED
        Instantiate(i.msgPrefab, i.msgParent).GetComponent<Text>().text = msg;
#endif
    }
}
