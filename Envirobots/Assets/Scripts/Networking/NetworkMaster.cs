﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using System;

public class NetworkMaster : Photon.PunBehaviour {

    /// <summary>
    /// string is enemy data
    /// </summary>
    public static event Action<string, ExitGames.Client.Photon.Hashtable> RecievedTransmission;
    public static event Action ConnectionComplete;

    static NetworkMaster i;

    const string _gameVersion = "0";

    public static ushort UID { get { return playerUID; } }
    static ushort playerUID;

    public static bool connected;
    static bool gameRoomExists = false;


    public static bool isMaster;

    static ushort cachedUID;

    private void Awake()
    {
        connected = false;
        i = this;
        playerUID = (ushort)UnityEngine.Random.Range(1, ushort.MaxValue);
        Connect();
    }
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        base.OnPhotonPlayerConnected(newPlayer);
        NetworkDebugging.Log(newPlayer.NickName);
    }
    public void ConnectToNetwork()
    {

    }

    public static void CreateRoom()
    {
        if (!connected)
            return;


        if (PhotonNetwork.JoinOrCreateRoom("the_gameroom", new RoomOptions() { MaxPlayers = 20 }, null))
        {

        }


    }


    private void Connect()
    {
        if (PhotonNetwork.connected)
        {
            Debug.Log("connected to network");
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }



    public override void OnConnectedToMaster()
    {
        NetworkDebugging.Log("connected to master");
        connected = true;
        PhotonNetwork.autoJoinLobby = false;
        foreach (RoomInfo r in PhotonNetwork.GetRoomList())
        {
            NetworkDebugging.Log(r.Name);
            if (r.Name == "the_gameroom")
            {
                gameRoomExists = true;
            }
        }
    }

    public override void OnDisconnectedFromPhoton()
    {
        base.OnDisconnectedFromPhoton();
        connected = false;
    }

    public override void OnJoinedRoom()
    {
        NetworkDebugging.Log("This is now in a room: " + PhotonNetwork.room.Name);
        ExitGames.Client.Photon.Hashtable h = PhotonNetwork.room.CustomProperties;
        if (!h.ContainsKey("initialized"))
        {
            h = new ExitGames.Client.Photon.Hashtable();
            NetworkDebugging.Log("connecting as chef");
            h.Add("initialized", true);
            h.Add("cached_id", 0);
            isMaster = true;
        }
        else
        {
            h = PhotonNetwork.room.CustomProperties;
            NetworkDebugging.Log("connecting as regular dude");
            while (h.ContainsKey(playerUID.ToString()))
            {
                playerUID = (ushort)UnityEngine.Random.Range(1, ushort.MaxValue);
            }

            h.Add(playerUID.ToString(), "");
            isMaster = false;
        }

        PhotonNetwork.room.SetCustomProperties(h);


        if (ConnectionComplete != null) ConnectionComplete();
    }

    public static void UpdateRobostring(string myRobot)
    {
        if (!connected)
        {
            NetworkDebugging.Log("you are not connected to the server");
            throw new System.Exception("you are not connected to the server");
        }
        if (PhotonNetwork.room == null)
        {
            NetworkDebugging.Log("you are not in a room");
            throw new System.Exception("You are not in a room");
        }

        ExitGames.Client.Photon.Hashtable h = PhotonNetwork.room.CustomProperties;
        h[playerUID.ToString()] = myRobot;
        PhotonNetwork.room.SetCustomProperties(h);
    }
    public static string GetRobostring()
    {
        if (!connected)
        {
            NetworkDebugging.Log("you are not connected to the server");
            throw new System.Exception("you are not connected to the server");
        }
        if (PhotonNetwork.room == null)
        {
            NetworkDebugging.Log("you are not in a room");
            throw new System.Exception("You are not in a room");
        }

        ExitGames.Client.Photon.Hashtable h = PhotonNetwork.room.CustomProperties;
        return (string)(h[playerUID.ToString()]);
    }

    public static string GetRobostringByHash(string uid)
    {
        if (!connected)
        {
            NetworkDebugging.Log("you are not connected to the server");
            throw new System.Exception("you are not connected to the server");
        }
        if (PhotonNetwork.room == null)
        {
            NetworkDebugging.Log("you are not in a room");
            throw new System.Exception("You are not in a room");
        }
        ExitGames.Client.Photon.Hashtable h = PhotonNetwork.room.CustomProperties;

        if (!h.ContainsKey(uid))
            throw new System.Exception("id not registered on server");

        return (string)(h[uid]);
    }

    public static void InitiateTransaction (string fromUID)
    {
        if (!connected)
        {
            NetworkDebugging.Log("you are not connected to the server");
            throw new System.Exception("you are not connected to the server");
        }
        if (PhotonNetwork.room == null)
        {
            NetworkDebugging.Log("you are not in a room");
            throw new System.Exception("You are not in a room");
        }

        i.DoTransation(fromUID);
    }

    void DoTransation(string fromUID)
    {
        NetworkDebugging.Log("doing tansaction...");        
        photonView.RPC("TransactionRPC", PhotonTargets.All, (playerUID.ToString() + "|" + fromUID));
    }
    [PunRPC]
    void TransactionRPC (string message)
    {
        string[] split = message.Split('|');
        ushort confirmUID = ushort.Parse(split[1]);
        ExitGames.Client.Photon.Hashtable h = PhotonNetwork.room.CustomProperties;

        if (playerUID != confirmUID)
        {
            return;
        }

        string otherString = (string)h[split[0]];
        string myString = (string)h[playerUID.ToString()];

        if (RecievedTransmission != null)
            RecievedTransmission(otherString, h);

        NetworkDebugging.Log("recieved message from: " + otherString + "my is: " + myString);
    }

}
